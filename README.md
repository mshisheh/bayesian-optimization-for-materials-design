# Bayesian Optimization for materials design



## Citrine Informatics Challenge:

•	The folder contains four ipynb files, two csv files and one jason file. 
•	I have solved two different problems as described below:
1-	Maximizing the bulk modulus of ternary alloy of M2AX which is a popular materials design problem where Orbital Radii of atoms has been suggested as the features (data_M.csv file). However, I have also used pymatgen and matminer libraries to generate another set of features based on Stoichiometry, ElementProperty, ValenceOrbital and IonProperty.
2-	Maximizing the Yield strength of steel from matbench datsets as suggested by Citrine. In the data set steels with almost same atom types, but different fractions are presented. Therefore, I took the fractions of atom types as the features.

## Models
•	I have used Two different suroggate model, (1) Gaussian process (GP) and (2) Random Frorest Regression model from lolopy library which provides uncertainty estimate of the model as well.
•	I have tested various acquisition functions suggested by citrine and those that are more popular such as probability of Improvement, upper confidence bound and maximum expected improvement. 
•	The BO were performed at least 30 times with different seed number to find the average number of steps to find the maximum and compared with Random selection.
•	Both RF and GP show promising results. The results show that, typically, the BO finds the maximum in less than 25 steps (10 initial and 15 BO) while for random selection it takes 100-150 times on average. 

## Files
Genearte_M2AX_data_Matminer: Use matminer and pymatgen to generate feature vectors for M2AX alloys
 Sequential_learning_Bulk_Modulus_M2AX_Orbitals: BO for finding the maximum bulk modulus of the M2AX alloy using radii of orbitals. 
Sequential_learning_Bulk_Modulus_M2AX_Matminer: BO for finding the maximum bulk modulus of the M2AX alloy using features from matminer

Sequential_learning_Yield_Strength: BO for finding the maximum yield strength of steel.

data_M.csv: file that contains the alloys, bulk modulus and radii of orbitals for atoms
data_Matminer: Generated csv file from Genearte_M2AX_data_Matminer 

steels_yield.jason: Jason file downloaded from matbench datsets
